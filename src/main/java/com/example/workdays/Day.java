package com.example.workdays;

import lombok.Data;

import java.time.LocalDate;

@Data
public class Day {
    private final LocalDate date;
    private final Integer t;
    private final Integer h;
}
