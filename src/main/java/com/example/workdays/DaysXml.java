package com.example.workdays;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@Data
@XmlRootElement(name = "days")
@XmlAccessorType(XmlAccessType.FIELD)
public class DaysXml {
    @XmlElement(name = "day")
    private List<DayXml> days = new ArrayList<>();
}
