package com.example.workdays;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@XmlRootElement(name = "day")
@XmlAccessorType(XmlAccessType.FIELD)
public class DayXml {
    @XmlAttribute
    private String d;

    @XmlAttribute
    private Integer t;

    @XmlAttribute
    private Integer h;
}
