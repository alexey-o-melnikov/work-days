package com.example.workdays;

import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Month;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.time.DayOfWeek.*;

@SpringBootApplication
public class WorkDaysApplication {

    public static void main(String[] args) {
        //SpringApplication.run(WorkDaysApplication.class, args);

        List<DayOfWeek> mnFr = Arrays.asList(MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY);
        List<DayOfWeek> tsSt = Arrays.asList(TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY);

        try {
            ClassLoader classLoader = ClassLoader.getSystemClassLoader();
            File file = new File(classLoader.getResource("days.xml").getFile());
            JAXBContext jaxbContext = JAXBContext.newInstance(DaysXml.class);

            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            DaysXml daysXml = (DaysXml) jaxbUnmarshaller.unmarshal(file);
//            System.out.println(daysXml.getDays());

            List<Day> hollyDays = daysXml.getDays().stream()
                    .map(dx -> {
                                String[] d = dx.getD().split("\\.");
                                int day = Integer.parseInt(d[1]);
                                int month = Integer.parseInt(d[0]);
                                LocalDate date = LocalDate.of(2018, month, day);

                                return new Day(date, dx.getT(), dx.getH());
                            }
                    ).filter(d -> d.getT().equals(1))
                    .collect(Collectors.toList());

//            System.out.println(hollyDays);

            List<LocalDate> dayOfyear = new ArrayList<>();
            for (int m = 0; m < 12; m++) {
                Calendar cal = Calendar.getInstance();
                cal.set(Calendar.MONTH, m);
                cal.set(Calendar.DAY_OF_MONTH, 1);
                int maxDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH);

                for (int i = 1; i < maxDay; i++) {
                    cal.set(Calendar.DAY_OF_MONTH, i + 1);
                    LocalDate date = cal.getTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
//                    System.out.println(date);
                    dayOfyear.add(date);
                }
            }

            System.out.println("пн-пт=" + getMonthIntegerMap(mnFr, hollyDays, dayOfyear));
            System.out.println("вт-сб=" + getMonthIntegerMap(tsSt, hollyDays, dayOfyear));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static Map<Month, Integer> getMonthIntegerMap(List<DayOfWeek> dayOfWeeks, List<Day> hollyDays, List<LocalDate> dayOfYear) {
        return dayOfYear.stream()
                .filter(d -> dayOfWeeks.contains(d.getDayOfWeek()))
                .filter(d -> !hollyDays.stream().map(Day::getDate).collect(Collectors.toList()).contains(d))
                .collect(Collectors.groupingBy(
                        LocalDate::getMonth,
                        Collectors.reducing(
                                0,
                                d -> 1,
                                Integer::sum
                        )));
    }
}
